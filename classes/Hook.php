<?php
/**
 *
 * @file
 * @ingroup Extensions
 * @link		http://www.mediawiki.org/wiki/Extension:DynamicPageList_(third-party)	Documentation
 * @author		n:en:User:IlyaHaykinson
 * @author		n:en:User:Amgine
 * @author		w:de:Benutzer:Unendlich
 * @author		m:User:Dangerman <cyril.dangerville@gmail.com>
 * @author		m:User:Algorithmix <gero.scholz@gmx.de>
 * @license		GPL-2.0-or-later
 *
 */

namespace DPL;

use Parser;
use PPFrame;
use DatabaseUpdater;
use Wikimedia\AtEase\AtEase;

class Hook {

	// $1: 'namespace' or 'notnamespace'
	// $2: wrong parameter given by user
	// $3: list of possible titles of namespaces (except pseudo-namespaces: Media, Special)
	public const FATAL_WRONGNS = 1001;

	// $1: linksto'
	// $2: the wrong parameter given by user
	public const FATAL_WRONGLINKSTO = 1002;

	// $1: max number of categories that can be included
	public const FATAL_TOOMANYCATS = 1003;

	// $1: min number of categories that have to be included
	public const FATAL_TOOFEWCATS = 1004;

	public const FATAL_NOSELECTION = 1005;

	public const FATAL_CATDATEBUTNOINCLUDEDCATS = 1006;

	public const FATAL_CATDATEBUTMORETHAN1CAT = 1007;

	public const FATAL_MORETHAN1TYPEOFDATE = 1008;

	// $1: param=val that is possible only with $1 as last 'ordermethod' parameter
	// $2: last 'ordermethod' parameter required for $0
	public const FATAL_WRONGORDERMETHOD = 1009;

	// $1: the number of arguments in includepage
	public const FATAL_DOMINANTSECTIONRANGE = 1010;

	public const FATAL_OPENREFERENCES = 1012;

	public const FATAL_MISSINGPARAMFUNCTION = 1022;

	public const FATAL_NOTPROTECTED = 1023;

	public const FATAL_SQLBUILDERROR = 1024;

	// ERROR

	// WARN

	// $1: unknown parameter given by user
	// $2: list of DPL available parameters separated by ', '
	public const WARN_UNKNOWNPARAM = 2013;

	// $1: Parameter given by user
	public const WARN_PARAMNOOPTION = 2022;

	// $3: list of valid param values separated by ' | '
	public const WARN_WRONGPARAM = 2014;

	// $1: param name
	// $2: wrong param value given by user
	// $3: default param value used instead by program
	public const WARN_WRONGPARAM_INT = 2015;

	public const WARN_NORESULTS = 2016;

	public const WARN_CATOUTPUTBUTWRONGPARAMS = 2017;

	// $1: 'headingmode' value given by user
	// $2: value used instead by program (which means no heading)
	public const WARN_HEADINGBUTSIMPLEORDERMETHOD = 2018;

	// $1: 'log' value
	public const WARN_DEBUGPARAMNOTFIRST = 2019;

	// $1: title of page that creates an infinite transclusion loop
	public const WARN_TRANSCLUSIONLOOP = 2020;

	// INFO

	// DEBUG

	// $1: SQL query executed to generate the dynamic page list
	public const DEBUG_QUERY = 3021;

	// TRACE

	/**
	 * number of articles
	 *
	 * @var array<string,int>
	 */
	public static $fixedCategories = [];

	/**
	 * the links created by DPL are collected here;
	 * they can be removed during the final ouput
	 * phase of the MediaWiki parser
	 *
	 * @var array<string,bool>
	 */
	public static $createdLinks;

	/**
	 * DPL acting like Extension:Intersection
	 *
	 * @var bool
	 */
	private static $likeIntersection = false;

	/**
	 * Debugging Level
	 *
	 * @var int
	 */
	private static $debugLevel = 0;

	/**
	 * Handle special on extension registration bits.
	 */
	public static function onRegistration(): void {
		if ( !defined( 'DPL_VERSION' ) ) {
			define( 'DPL_VERSION', '3.3.3' );
		}
	}

	/**
	 * Sets up this extension's parser functions.
	 *
	 * @param Parser $parser
	 */
	public static function onParserFirstCallInit( Parser $parser ): void {
		self::init();

		// DPL offers the same functionality as Intersection.  So we register the <DynamicPageList>
		// tag in case LabeledSection Extension is not installed so that the section markers are
		// removed.
		if ( Config::getSetting( 'handleSectionTag' ) ) {
			$parser->setHook( 'section',			[ __CLASS__, 'dplTag' ] );
		}
		$parser->setHook( 'DPL',					[ __CLASS__, 'dplTag' ] );
		$parser->setHook( 'DynamicPageList',		[ __CLASS__, 'intersectionTag' ] );

		$parser->setFunctionHook( 'dpl',			[ __CLASS__, 'dplParserFunction' ] );
		$parser->setFunctionHook( 'dplnum',		[ __CLASS__, 'dplNumParserFunction' ] );
		$parser->setFunctionHook( 'dplvar',		[ __CLASS__, 'dplVarParserFunction' ] );
		$parser->setFunctionHook( 'dplreplace',	[ __CLASS__, 'dplReplaceParserFunction' ] );
		$parser->setFunctionHook( 'dplchapter',	[ __CLASS__, 'dplChapterParserFunction' ] );
		$parser->setFunctionHook( 'dplmatrix',	[ __CLASS__, 'dplMatrixParserFunction' ] );
	}

	/**
	 * Sets up this extension's parser functions for migration from Intersection.
	 *
	 * @param Parser $parser
	 */
	public static function setupMigration( Parser $parser ): void {
		$parser->setHook( 'Intersection', [ __CLASS__, 'intersectionTag' ] );
		$parser->addTrackingCategory( 'dpl-intersection-tracking-category' );

		self::init();
	}

	/**
	 * Common initializer for usage from parser entry points.
	 */
	private static function init(): void {
		Config::init();

		if ( !isset( self::$createdLinks ) ) {
			self::$createdLinks = [
				'resetLinks'		=> false,
				'resetTemplates'	=> false,
				'resetCategories'	=> false,
				'resetImages'		=> false,
				'resetdone'			=> false,
				'elimdone'			=> false
			];
		}
	}

	/**
	 * Set to behave like intersection.
	 *
	 * @param bool $mode Behave Like Intersection
	 */
	private static function setLikeIntersection( bool $mode = false ): void {
		self::$likeIntersection = $mode;
	}

	/**
	 * Is like intersection?
	 */
	public static function isLikeIntersection(): bool {
		return (bool)self::$likeIntersection;
	}

	/**
	 * Tag <section> entry point.
	 *
	 * @param string $input Raw User Input
	 * @param array $args Arguments on the tag.
	 * @param Parser $parser
	 * @param PPFrame $frame
	 */
	public static function intersectionTag( string $input, array $args, Parser $parser, PPFrame $frame ): string {
		self::setLikeIntersection( true );
		return self::executeTag( $input, $args, $parser, $frame );
	}

	/**
	 * Tag <dpl> entry point.
	 *
	 * @param string $input Raw User Input
	 * @param array $args Arguments on the tag.
	 * @param Parser $parser
	 * @param PPFrame $frame
	 */
	public static function dplTag( $input, array $args, Parser $parser, PPFrame $frame ): string {
		self::setLikeIntersection( false );
		$parser->addTrackingCategory( 'dpl-tag-tracking-category' );
		return self::executeTag( $input, $args, $parser, $frame );
	}

	/**
	 * The callback function wrapper for converting the input text to HTML output
	 *
	 * @param string $input Raw User Input
	 * @param array $args Arguments on the tag.
	 * @param Parser $parser
	 * @param PPFrame $frame
	 */
	private static function executeTag( $input, array $args, Parser $parser, PPFrame $frame ): string {
		// entry point for user tag <dpl>  or  <DynamicPageList>
		// create list and do a recursive parse of the output

		$parse = new Parse();
		if ( Config::getSetting( 'recursiveTagParse' ) ) {
			$input = $parser->recursiveTagParse( $input, $frame );
		}
		$text = $parse->parse( $input, $parser, $reset, $eliminate, true );

		if ( isset( $reset['templates'] ) && $reset['templates'] ) {
			// we can remove the templates by save/restore
			$saveTemplates = $parser->mOutput->mTemplates;
		}
		if ( isset( $reset['categories'] ) && $reset['categories'] ) {
			// we can remove the categories by save/restore
			$saveCategories = $parser->mOutput->mCategories;
		}
		if ( isset( $reset['images'] ) && $reset['images'] ) {
			// we can remove the images by save/restore
			$saveImages = $parser->mOutput->mImages;
		}
		$parsedDPL = $parser->recursiveTagParse( $text );
		if ( isset( $reset['templates'] ) && $reset['templates'] ) {
			$parser->mOutput->mTemplates = $saveTemplates;
		}
		if ( isset( $reset['categories'] ) && $reset['categories'] ) {
			$parser->mOutput->mCategories = $saveCategories;
		}
		if ( isset( $reset['images'] ) && $reset['images'] ) {
			$parser->mOutput->mImages = $saveImages;
		}

		return $parsedDPL;
	}

	/**
	 * The #dpl parser tag entry point.
	 *
	 * @param Parser $parser
	 */
	public static function dplParserFunction( Parser $parser ): array {
		self::setLikeIntersection( false );

		$parser->addTrackingCategory( 'dpl-parserfunc-tracking-category' );

		// callback for the parser function {{#dpl:	  or   {{DynamicPageList::
		$input = "";

		$numargs = func_num_args();
		if ( $numargs < 2 ) {
			$input = "#dpl: no arguments specified";
			return str_replace( '§', '<', '§pre>§nowiki>' . $input . '§/nowiki>§/pre>' );
		}

		// fetch all user-provided arguments (skipping $parser)
		$arg_list = func_get_args();
		for ( $i = 1; $i < $numargs; $i++ ) {
			$p1 = $arg_list[$i];
			$input .= str_replace( "\n", "", $p1 ) . "\n";
		}

		$parse = new Parse();
		$dplresult = $parse->parse( $input, $parser, $reset, $eliminate, false );

		// parser needs to be coaxed to do further recursive processing
		return [
			$parser->getPreprocessor()->preprocessToObj( $dplresult, Parser::PTD_FOR_INCLUSION ),
			'isLocalObj' => true,
			'title' => $parser->getTitle()
		];
	}

	/**
	 * The #dplnum parser tag entry point.
	 *
	 * From the old documentation:
	 *     Tries to guess a number that is buried in the text.  Uses a set of heuristic rules which
	 *     may work or not.  The idea is to extract the number so that it can be used as a sorting
	 *     value in the column of a DPL table output.
	 *
	 * @param Parser $parser
	 * @param string $text
	 */
	public static function dplNumParserFunction( Parser $parser, string $text = '' ): string {
		$parser->addTrackingCategory( 'dplnum-parserfunc-tracking-category' );
		$num = str_replace( '&#160;', ' ', $text );
		$num = str_replace( '&nbsp;', ' ', $text );
		$num = preg_replace( '/([0-9])([.])([0-9][0-9]?[^0-9,])/', '\1,\3', $num );
		$num = preg_replace( '/([0-9.]+),([0-9][0-9][0-9])\s*Mrd/', '\1\2 000000 ', $num );
		$num = preg_replace( '/([0-9.]+),([0-9][0-9])\s*Mrd/', '\1\2 0000000 ', $num );
		$num = preg_replace( '/([0-9.]+),([0-9])\s*Mrd/', '\1\2 00000000 ', $num );
		$num = preg_replace( '/\s*Mrd/', '000000000 ', $num );
		$num = preg_replace( '/([0-9.]+),([0-9][0-9][0-9])\s*Mio/', '\1\2 000 ', $num );
		$num = preg_replace( '/([0-9.]+),([0-9][0-9])\s*Mio/', '\1\2 0000 ', $num );
		$num = preg_replace( '/([0-9.]+),([0-9])\s*Mio/', '\1\2 00000', $num );
		$num = preg_replace( '/\s*Mio/', '000000 ', $num );
		$num = preg_replace( '/[. ]/', '', $num );
		$num = preg_replace( '/^[^0-9]+/', '', $num );
		$num = preg_replace( '/[^0-9].*/', '', $num );
		return $num;
	}

	/**
	 * (1) assigns the values to symbolic variable names.
	 * (2) assigns the value only if the variable is empty / has not been used so far.
	 * (3) returns the current value of the variable.
	 *
	 * The variables can be set and retrieved from an article page or from templates used in that
	 * article.
	 *
	 * @param Parser $parser
	 * @param string $init cmd to execute or var to get
	 * @param string[] ...$args any other arguments
	 */
	public static function dplVarParserFunction( Parser $parser, $init, ...$args ) {
		$parser->addTrackingCategory( 'dplvar-parserfunc-tracking-category' );
		$cmd = null;
		if ( $init === "set" ) {
			$ret = Variables::setVar( $args );
		} elseif ( $init === "default" ) {
			$ret = Variables::setVarDefault( $args );
		} else {
			$ret = Variables::getVar( $init );
		}
		return $ret;
	}

	/**
	 * internal check to see if $needle is a regular expression.
	 *
	 * @param string $needle to check
	 */
	private static function isRegexp( string $needle ): bool {
		if ( strlen( $needle ) < 3 ) {
			return false;
		}
		if ( ctype_alnum( $needle[0] ) ) {
			return false;
		}
		$nettoNeedle = preg_replace( '/[ismu]*$/', '', $needle );
		if ( strlen( $nettoNeedle ) < 2 ) {
			return false;
		}
		if ( $needle[0] == $nettoNeedle[strlen( $nettoNeedle ) - 1] ) {
			return true;
		}
		return false;
	}

	/**
	 * Replaces the given pattern within the text by replacement.
	 *
	 * @param Parser $parser
	 * @param string $text to operate on
	 * @param string $pat pattern to search for
	 * @param string $repl to replace pattern
	 */
	public static function dplReplaceParserFunction(
		Parser $parser,
		string $text,
		string $pat = '',
		string $repl = ''
	) {
		$parser->addTrackingCategory( 'dplreplace-parserfunc-tracking-category' );
		if ( $text == '' || $pat == '' ) {
			return '';
		}
		# convert \n to a real newline character
		$repl = str_replace( '\n', "\n", $repl );

		# replace
		if ( !self::isRegexp( $pat ) ) {
			$pat = '`' . str_replace( '`', '\`', $pat ) . '`';
		}

		AtEase::suppressWarnings();
		$ret = preg_replace( $pat, $repl, $text );
		AtEase::restoreWarnings();

		return $ret;
	}

	/**
	 * You pass an arbitrary text to this function and you will get back the body of a chapter
	 * within this text which has 'heading' as a headline. You can limit the output to a maximum
	 * number of characters. You will get a link to 'page' if the text is longer than that
	 * limit. The link will normally consist of some arrow but can be changed to whatever you want.
	 *
	 * @param Parser $parser
	 * @param string $text to operate on
	 * @param string $heading
	 * @param string $maxLength
	 * @param string $page
	 * @param string $link
	 */
	public static function dplChapterParserFunction(
		Parser $parser,
		string $text = '',
		string $heading = ' ',
		string $maxLength = '',
		string $page = '?page?',
		string $link = 'default'
	): string {
		$parser->addTrackingCategory( 'dplchapter-parserfunc-tracking-category' );
		$output = LST::extractHeadingFromText(
			$parser, $page, '?title?', $text, $heading, '', $sectionHeading, true, intval( $maxLength ), $link, false
		);
		return $output[0];
	}

	/**
	 * You pass an indented list and you get a matrix view back. With copy/paste you can easily
	 * transfer such a matrix to Excel and do some polishing there like turning column texts to
	 * vertical, highlight interesting cells etc..)
	 *
	 * @param Parser $parser
	 * @param string $name of the matrix
	 * @param string $yes The symbol for cells which symbolize a link. Default is "x".
	 * @param string $no The symbol for cells which symbolize the absence of a link. Default is 'empty cell'.
	 * @param string $flip The mode ('normal' or 'flip'); default is 'normal'. When 'flip' is
	 * specified rows and columns will be exchanged.
	 * @param string $matrix The indented list:
	 *   - source items must start in column 1;
	 *   - target items must be indented by at least one space
	 *   - after the item name you can add ~~ and a 'label'. Row and column titles of the matrix
	 *         will contain hyperlinks to the items. If lables are specified they will be used
	 *         instead of the item names.
	 *   - lines which are empty or contain only spaces are silently ignored.
	 *   - if a source line is not followed by one or more target lines an empty table row (or column
	 *     in flip mode) will be shown.
	 *   - if the same source line appears multiple times, the targets will be joined (added).
	 */
	public static function dplMatrixParserFunction(
		Parser $parser,
		string $name = '',
		string $yes = '',
		string $no = '',
		string $flip = '',
		string $matrix = ''
	): string {
		$parser->addTrackingCategory( 'dplmatrix-parserfunc-tracking-category' );
		$lines   = explode( "\n", $matrix );
		$m       = [];
		$sources = [];
		$targets = [];
		$from    = '';
		$to      = '';
		if ( $flip == '' | $flip == 'normal' ) {
			$flip = false;
		} else {
			$flip = true;
		}
		if ( $name == '' ) {
			$name = '&#160;';
		}
		if ( $yes == '' ) {
			$yes = ' x ';
		}
		if ( $no == '' ) {
			$no = '&#160;';
		}
		if ( $no[0] == '-' ) {
			$no = " $no ";
		}
		foreach ( $lines as $line ) {
			if ( strlen( $line ) <= 0 ) {
				continue;
			}
			if ( $line[0] != ' ' ) {
				$from = preg_split( ' *\~\~ *', trim( $line ), 2 );
				if ( !array_key_exists( $from[0], $sources ) ) {
					if ( count( $from ) < 2 || $from[1] == '' ) {
						$sources[$from[0]] = $from[0];
					} else {
						$sources[$from[0]] = $from[1];
					}
					$m[$from[0]] = [];
				}
			} elseif ( trim( $line ) != '' ) {
				$to = preg_split( ' *\~\~ *', trim( $line ), 2 );
				if ( count( $to ) < 2 || $to[1] == '' ) {
					$targets[$to[0]] = $to[0];
				} else {
					$targets[$to[0]] = $to[1];
				}
				$m[$from[0]][$to[0]] = true;
			}
		}
		ksort( $targets );

		$header = "\n";

		if ( $flip ) {
			foreach ( $sources as $from => $fromName ) {
				$header .= "![[$from|" . $fromName . "]]\n";
			}
			foreach ( $targets as $to => $toName ) {
				$targets[$to] = "[[$to|$toName]]";
				foreach ( $sources as $from => $fromName ) {
					if ( array_key_exists( $to, $m[$from] ) ) {
						$targets[$to] .= "\n|$yes";
					} else {
						$targets[$to] .= "\n|$no";
					}
				}
				$targets[$to] .= "\n|--\n";
			}
			return "{|class=dplmatrix\n|$name" . "\n" . $header . "|--\n!" . implode( "\n!", $targets ) . "\n|}";
		} else {
			foreach ( $targets as $to => $toName ) {
				$header .= "![[$to|" . $toName . "]]\n";
			}
			foreach ( $sources as $from => $fromName ) {
				$sources[$from] = "[[$from|$fromName]]";
				foreach ( $targets as $to => $toName ) {
					if ( array_key_exists( $to, $m[$from] ) ) {
						$sources[$from] .= "\n|$yes";
					} else {
						$sources[$from] .= "\n|$no";
					}
				}
				$sources[$from] .= "\n|--\n";
			}
			return "{|class=dplmatrix\n|$name" . "\n" . $header . "|--\n!" . implode( "\n!", $sources ) . "\n|}";
		}
	}

	/**
	 * Internal debugging function
	 *
	 * @param Parser $parser
	 * @param string $label
	 */
	private static function dumpParsedRefs( $parser, $label ): void {
		echo '<pre>parser mLinks: ';
		ob_start();
		var_dump( $parser->mOutput->mLinks );
		$a = ob_get_contents();
		ob_end_clean();
		echo htmlspecialchars( $a, ENT_QUOTES );
		echo '</pre>';
		echo '<pre>parser mTemplates: ';
		ob_start();
		var_dump( $parser->mOutput->mTemplates );
		$a = ob_get_contents();
		ob_end_clean();
		echo htmlspecialchars( $a, ENT_QUOTES );
		echo '</pre>';
	}

	/**
	 * Remove section markers in case the LabeledSectionTransclusion extension is not installed.
	 *
	 * @param string $in
	 * @param array $assocArgs
	 * @param ?Parser $parser
	 */
	public static function removeSectionMarkers( string $in, array $assocArgs, ?Parser $parser ) {
		return '';
	}

	/**
	 * @param string $cat category
	 */
	public static function fixCategory( string $cat ): void {
		if ( $cat != '' ) {
			self::$fixedCategories[$cat] = 1;
		}
	}

	/**
	 * Set Debugging Level
	 *
	 * @param string $level Debug Level
	 */
	public static function setDebugLevel( string $level ): void {
		self::$debugLevel = intval( $level );
	}

	/**
	 * Return Debugging Level
	 */
	public static function getDebugLevel(): int {
		return self::$debugLevel;
	}

	/**
	 * Reset everything; some categories may have been fixed, however via fixcategory=
	 *
	 * @param Parser $parser
	 * @param string $text
	 */
	public static function endReset( Parser $parser, string $text ): bool {
		if ( !self::$createdLinks['resetdone'] ) {
			self::$createdLinks['resetdone'] = true;
			foreach ( $parser->mOutput->mCategories as $key => $val ) {
				if ( array_key_exists( $key, self::$fixedCategories ) ) {
					self::$fixedCategories[$key] = $val;
				}
			}
			// $text .= self::dumpParsedRefs($parser,"before final reset");
			if ( self::$createdLinks['resetLinks'] ) {
				$parser->mOutput->mLinks = [];
			}
			if ( self::$createdLinks['resetCategories'] ) {
				$parser->mOutput->mCategories = self::$fixedCategories;
			}
			if ( self::$createdLinks['resetTemplates'] ) {
				$parser->mOutput->mTemplates = [];
			}
			if ( self::$createdLinks['resetImages'] ) {
				$parser->mOutput->mImages = [];
			}
			// $text .= self::dumpParsedRefs($parser,"after final reset");
			self::$fixedCategories = [];
		}
		return true;
	}

	/**
	 * @param Parser $parser
	 * @param string &$text
	 */
	public static function endEliminate( Parser $parser, string &$text ) {
		// called during the final output phase; removes links created by DPL
		if ( isset( self::$createdLinks ) ) {
			// self::dumpParsedRefs($parser,"before final eliminate");
			if ( array_key_exists( 0, self::$createdLinks ) ) {
				foreach ( $parser->mOutput->getLinks() as $nsp => $link ) {
					if ( !array_key_exists( $nsp, self::$createdLinks[0] ) ) {
						continue;
					}
					$parser->mOutput->mLinks[$nsp] = array_diff_assoc(
						$parser->mOutput->mLinks[$nsp], self::$createdLinks[0][$nsp]
					);
					if ( count( $parser->mOutput->mLinks[$nsp] ) == 0 ) {
						unset( $parser->mOutput->mLinks[$nsp] );
					}
				}
			}
			if ( isset( self::$createdLinks ) && array_key_exists( 1, self::$createdLinks ) ) {
				foreach ( $parser->mOutput->mTemplates as $nsp => $tpl ) {
					if ( !array_key_exists( $nsp, self::$createdLinks[1] ) ) {
						continue;
					}
					$parser->mOutput->mTemplates[$nsp] = array_diff_assoc(
						$parser->mOutput->mTemplates[$nsp], self::$createdLinks[1][$nsp]
					);
					if ( count( $parser->mOutput->mTemplates[$nsp] ) == 0 ) {
						unset( $parser->mOutput->mTemplates[$nsp] );
					}
				}
			}
			if ( isset( self::$createdLinks ) && array_key_exists( 2, self::$createdLinks ) ) {
				$parser->mOutput->mCategories = array_diff_assoc(
					$parser->mOutput->mCategories, self::$createdLinks[2]
				);
			}
			if ( isset( self::$createdLinks ) && array_key_exists( 3, self::$createdLinks ) ) {
				$parser->mOutput->mImages = array_diff_assoc(
					$parser->mOutput->mImages, self::$createdLinks[3]
				);
			}
		}
	}

	/**
	 * Setups and Modifies Database Information
	 *
	 * @param DatabaseUpdater $updater
	 */
	public static function onLoadExtensionSchemaUpdates( DatabaseUpdater $updater ) {
		$extDir = __DIR__;

		$updater->addPostDatabaseUpdateMaintenance( 'DPL\\DB\\CreateTemplateUpdateMaintenance' );

		$db = $updater->getDB();
		if ( !$db->tableExists( 'dpl_clview' ) ) {
			// PostgreSQL doesn't have IFNULL, so use COALESCE instead
			$sqlNullMethod = ( $db->getType() === 'postgres' ? 'COALESCE' : 'IFNULL' );
			$db->query(
				"CREATE VIEW {$db->tablePrefix()}dpl_clview AS "
				. "SELECT $sqlNullMethod(cl_from, page_id) AS cl_from, "
				. "$sqlNullMethod(cl_to, '') AS cl_to, cl_sortkey "
				. "FROM {$db->tablePrefix()}page "
				. "LEFT OUTER JOIN {$db->tablePrefix()}categorylinks "
				. "ON {$db->tablePrefix()}page.page_id=cl_from;"
			);
		}
	}
}
